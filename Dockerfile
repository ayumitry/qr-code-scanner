FROM node:16.16.0-alpine as node

WORKDIR /app

# Copy package.json and pnpm-lock.yaml to the working directory
COPY package.json pnpm-lock.yaml ./

# Install pnpm globally
RUN npm install -g pnpm

# Install dependencies using pnpm
RUN pnpm install

RUN npm install -g serve


# Copy the rest of the application code
COPY . .

# run build
RUN pnpm run build

# Build and run the application
CMD ["serve", "-p", "5173", "-s", "."]


# FROM node:16.16.0-alpine as node

# WORKDIR /app

# # Copy files to app

# COPY package.json tailwind.config.cjs postcss.config.cjs vite.config.js sw.js pnpm-lock.yaml index.html /app/
# COPY ./src /app/src
# COPY ./public /app/public
# COPY ./node_modules /app/node_modules

# # run 
# RUN npm install
# RUN npm start

# # Expose the port
# EXPOSE 5173

# # Start the application
# CMD ["npm", "start"]
