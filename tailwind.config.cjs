/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx,ts,tsx}"],
  theme: {
    extend: {},
    fontFamily: {
      dmsans: '"DM Sans", sans-serif',
      poppins: 'Poppins, sans-serif',
      mplus: '"M PLUS Rounded 1c", sans-serif'
    }
  },
  plugins: [],
  important: true
}
