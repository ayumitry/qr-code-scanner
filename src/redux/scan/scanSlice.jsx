import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import {
  GET_TICKET,
  REGISTER_BOOKING,
  PAYMENT_UPDATED,
  DETAIL_BOOKING
} from '../../constans'

export const bookingPayment = createAsyncThunk(
  'scan/bookingPayment',
  async payload => {
    const response = await fetch(REGISTER_BOOKING, {
      method: 'POST',
      body: JSON.stringify(payload)
    })
    const json = await response.json()
    return json
  }
)

export const paymentUpdated = createAsyncThunk(
  'scan/paymentUpdated',
  async payload => {
    const response = await fetch(PAYMENT_UPDATED, {
      method: 'POST',
      body: JSON.stringify(payload)
    })
    return response.json()
  }
)

export const scanTicket = createAsyncThunk('scan/scanTicket', async id => {
  const response = await fetch(GET_TICKET + id)
  const result = await response.json()
  return result
})

export const detailBooking = createAsyncThunk(
  'scan/detailBooking',
  async id => {
    const response = await fetch(DETAIL_BOOKING + id)
    const result = await response.json()
    return result
  }
)

const initialState = {
  updated: {},
  data: {
    data: [],
    isLoading: true
  }
}

export const scaneSlice = createSlice({
  name: 'scan',
  initialState,
  extraReducers: {
    [bookingPayment.fulfilled]: (state, action) => {
      state.data = {
        data: action.payload,
        isLoading: false
      }
    },
    [bookingPayment.pending]: state => {
      state.data = {
        ...state.data,
        isLoading: true
      }
    },
    [paymentUpdated.fulfilled]: (state, action) => {
      return {
        ...state,
        updated: { ...action.payload }
      }
    },
    [paymentUpdated.pending]: state => {},
    [scanTicket.fulfilled]: (state, action) => {
      state.data = {
        data: action.payload,
        isLoading: false
      }
    },
    [scanTicket.pending]: state => {
      state.data = {
        ...state.data,
        isLoading: true
      }
    },
    [detailBooking.fulfilled]: (state, action) => {
      state.data = {
        data: action.payload,
        isLoading: false
      }
    },
    [detailBooking.pending]: state => {
      state.data = {
        ...state.data,
        isLoading: true
      }
    }
  }
})

export default scaneSlice.reducer
