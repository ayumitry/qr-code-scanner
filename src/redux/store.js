import { configureStore } from "@reduxjs/toolkit";
import scanSlice from "./scan/scanSlice";

export const store = configureStore({
  reducer: {
    scan: scanSlice,
  },
});