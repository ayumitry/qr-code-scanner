export const GET_TICKET = 'https://sandbox-app.antrique.com/api-socket/service/view?id='
export const REGISTER_BOOKING = 'https://sandbox-app.antrique.com/api-socket/booking/create'
export const PAYMENT_UPDATED = 'https://sandbox-app.antrique.com/api-socket/booking/payment'
export const DETAIL_BOOKING = 'https://sandbox-app.antrique.com/api-socket/booking/detail?paymentId='
