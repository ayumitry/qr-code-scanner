import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { scanTicket } from '../../redux/scan/scanSlice'
import { useDispatch, useSelector } from 'react-redux'
import QrReader from 'react-qr-reader'

const Home = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const selector = useSelector(state => state.scan.data)

  const [startScan, setStartScan] = useState(false)
  const [data, setData] = useState(null)

  const handleError = err => {
    console.error(err)
  }

  const handleScan = async scanData => {
    if (scanData && scanData !== data) {
      dispatch(scanTicket(scanData))
      setData(scanData)
    }
  }

  useEffect(() => {
    if (data) {
      history.push({
        pathname: '/qrResult',
        state: selector
      })
    }
  }, [selector, history])

  useEffect(() => {
    if (startScan) {
      const elements = document.querySelector('section > section')
      const img = document.createElement('img')
      img.src = 'https://svgshare.com/i/phF.svg'
      elements.appendChild(img)
    } else {
      const elements = document.querySelector('section > section > img')

      if (elements) {
        elements.remove()
      }
    }
  }, [startScan])

  return (
    <div>
      {!startScan && (
        <main className='flex flex-col items-center justify-center h-screen'>
          <div className='flex flex-col justify-between gap-24'>
            <div className='align-middle mx-auto space-y-4'>
              <img src='/img/power-adavter.svg' alt='power adapter' />
              <h1 className='text-xl text-black font-mplus text-center font-bold'>
                VKP Power
              </h1>
            </div>
            <div className='grid place-items-center gap-7'>
              <p className='text-center text-md font-bold break-words max-w-xs w-44'>
                Pindai kode QR untuk mengaktifkan Power
              </p>

              <button
                className='rounded-full p-4 bg-[#F25E39]'
                onClick={async () => {
                  const stream = await navigator.mediaDevices.getUserMedia({
                    video: true,
                    audio: false
                  })
                  if (stream.active) {
                    setStartScan(!startScan)
                  }
                }}
              >
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  fill='none'
                  viewBox='0 0 24 24'
                  strokeWidth={1.5}
                  stroke='currentColor'
                  className='w-6 h-6'
                  color='white'
                >
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    d='M3.75 4.875c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5A1.125 1.125 0 013.75 9.375v-4.5zM3.75 14.625c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5a1.125 1.125 0 01-1.125-1.125v-4.5zM13.5 4.875c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5A1.125 1.125 0 0113.5 9.375v-4.5z'
                  />
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    d='M6.75 6.75h.75v.75h-.75v-.75zM6.75 16.5h.75v.75h-.75v-.75zM16.5 6.75h.75v.75h-.75v-.75zM13.5 13.5h.75v.75h-.75v-.75zM13.5 19.5h.75v.75h-.75v-.75zM19.5 13.5h.75v.75h-.75v-.75zM19.5 19.5h.75v.75h-.75v-.75zM16.5 16.5h.75v.75h-.75v-.75z'
                  />
                </svg>
              </button>
            </div>
          </div>
        </main>
      )}

      {startScan && (
        <>
          <QrReader
            facingMode={'user'}
            delay={1000}
            onError={handleError}
            onScan={handleScan}
          />
        </>
      )}
    </div>
  )
}
export default Home
