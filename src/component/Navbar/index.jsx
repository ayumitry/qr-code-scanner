const Navbar = () => {
  return (
    <>
      <div className='mx-auto md:w-1/2 lg:w-1/3 xl:w-1/4 fixed inset-x-0 top-0 shadow-lg'>
        <div className='bg-[#F25E39] px-6 py-4 text-lg flex justify-center w-full'>
          <p className='font-bold text-white'>VKP</p>
        </div>
      </div>
    </>
  )
}
export default Navbar
