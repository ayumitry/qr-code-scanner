import moment from 'moment'
import Swal from 'sweetalert2'
import { useLocation, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { detailBooking } from '../../redux/scan/scanSlice'
import Countdown from 'react-countdown'

const Card = () => {
  const location = useLocation()
  const dispatch = useDispatch()
  const history = useHistory()
  const selector = useSelector(state => state.scan.data)

  const onCompleted = () => {
    return Swal.fire({
      title: 'Waktu Penggunaan Habis',
      text: 'Penggunaan Smart Power Socket telah habis, jika butuh tambahan waktu silahkan klik ‘Gunakan lagi’',
      showCancelButton: true,
      confirmButtonClass:
        'bg-[#4F4F4F] hover:bg-[#4F4F4F]/85 text-white rounded-lg',
      cancelButtonClass:
        'bg-white text-[#4F4F4F] rounded-lg border border-[#4F4F4F] swal2-styled',
      confirmButtonText: 'OK',
      cancelButtonText: 'Gunakan lagi',
      customClass: {
        popup: 'pr-7 pl-5',
        title: 'text-[#F25E39] font-mplus',
        htmlContainer: 'font-mplus',
        actions: 'py-3 flex flex-col w-full',
        confirmButton: 'w-full my-2 p-2 rounded-3xl',
        cancelButton: 'w-full my-2 p-2 rounded-3xl'
      }
    }).then(result => {
      if (result.isConfirmed) {
        return history.push('/')
      }

      if (result.dismiss === 'cancel') {
        return history.push('/qrResult')
      }
    })
  }

  const renderer = ({ hours, minutes, seconds }) => {
    return (
      <div className='flex justify-center '>
        <div className='bg-[#F25E39] w-auto text-center text-4xl font-bold text-white rounded-lg p-4'>
          {hours.toString().padStart(2, '0')} :{' '}
          {minutes.toString().padStart(2, '0')} :{' '}
          {seconds.toString().padStart(2, '0')}
        </div>
      </div>
    )
  }

  useEffect(() => {
    dispatch(detailBooking(location.state.transaction))
  }, [])

  return (
    <main className='flex flex-col gap-2 h-screen py-20 p-7 font-mplus'>
      {!selector.isLoading ? (
        <div className='max-w-md p-6 bg-white border border-gray-200 rounded-3xl shadow-md grid gap-6'>
          <div className='border-solid border-b-[1px] font-bold0'>
            <div className='flex flex-col gap-3 mb-2'>
              <p className='text-xl font-bold'>
                {selector?.data?.message?.service_name}
              </p>
              <p className='text-sm'>
                {selector?.data?.message?.merchant_name}
              </p>
              <div className='flex justify-between'>
                <p className='text-sm'>
                  {selector?.data?.message?.booking_date}
                </p>
                <p className='text-sm'>
                  {selector?.data?.message?.time_start}-
                  {selector?.data?.message?.time_end}
                </p>
              </div>
              <div className='flex justify-between'>
                <p className='text-sm'>
                  {selector?.data?.message?.booking_name}
                </p>
                <p className='text-sm'>
                  {selector?.data?.message?.booking_phone}
                </p>
              </div>
            </div>
          </div>
          <p className='text-md text-center'>Sisa Waktu Penggunaan</p>
          {selector?.data?.message?.time_end && (
            <Countdown
              date={moment(
                selector?.data?.message?.time_end,
                'HH:mm'
              ).toISOString()}
              renderer={renderer}
              onComplete={onCompleted}
            />
          )}
          <p className='text-md text-center'>
            Jika membutuhkan waktu tambahan ulangi pindai kode QR dan lakukan
            pembayaran kembali
          </p>
        </div>
      ) : (
        <div className='flex items-center justify-center'>
          <p>Loading...</p>
        </div>
      )}
    </main>
  )
}
export default Card
