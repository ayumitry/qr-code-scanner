import 'react-phone-number-input/style.css'
import React, { useState } from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { bookingPayment, paymentUpdated } from '../../redux/scan/scanSlice'
import Input from 'react-phone-number-input/input'

const Example = () => {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm()
  const location = useLocation()
  const dispatch = useDispatch()
  const history = useHistory()
  const bookingService = location.state?.data

  console.log(bookingService)

  const onBookingPayment = (name, phoneNumber) => {
    const payload = {
      service_id: bookingService.data.id,
      name: name,
      phone: phoneNumber.slice(1)
    }
    return dispatch(bookingPayment(payload))
  }
  const onBookingAndPayment = value => {
    onBookingPayment(value.name, value.phoneNumber).then(result => {
      const midtrans_token = result.payload.midtrans_token
      window.snap.pay(midtrans_token, {
        onSuccess: function (result) {
          const payload = {
            payment_id: result.order_id,
            channel_name: result.payment_type,
            trx_id: result.transaction_id,
            status: 2
          }
          dispatch(paymentUpdated(payload))
          history.push({
            pathname: '/qrTransaction',
            state: {
              transaction: payload.payment_id
            }
          })
        },
        onPending: function (result) {
          alert('waiting for your payment!')
          console.log(result)
        },
        onError: function (result) {
          alert('payment failed!')
          console.log(result)
        },
        onClose: function () {
          alert('you closed the popup without finishing the payment')
        }
      })
    })
  }
  return (
    <form
      className='flex flex-col gap-40 h-screen py-20 p-7 font-mplus justify-between'
      onSubmit={handleSubmit(onBookingAndPayment)}
    >
      <div className='grid gap-3'>
        <div className='border-solid border-b-[1px]'>
          <p className='text-xl font-bold mb-2'>
            {bookingService?.data?.service_name}
          </p>
          <p className='text-sm mb-3'>{bookingService?.data?.merchant_name}</p>
        </div>
        <div className='grid gap-2 rounded bg-white'>
          <label className='text-xs text-black'>Nama Pengguna</label>
          <input
            type='text'
            id='small-input'
            className='block w-full p-2 border font-bold border-gray-300 rounded-lg placeholder:font-normal bg-white'
            placeholder='enter your name'
            {...register('name', { required: true })}
          />
          {errors?.name && (
            <p className='text-sm text-red-500'>Name is required</p>
          )}
        </div>
        <div className='grid gap-2 rounded bg-white'>
          <label className='text-xs text-black'>Nomor Whatsapp</label>
          <Input
            international
            withCountryCallingCode
            country='ID'
            className='block w-full p-2 border border-gray-300 rounded-lg bg-white text-md font-bold '
            {...register('phoneNumber', {
              minLength: 4
            })}
          />
          {errors?.phoneNumber && (
            <p className='text-sm text-red-500'>Phone Number is required</p>
          )}
        </div>
        <div className='block max-w-md p-3 bg-[#FFF3E8] border border-gray-200 rounded-lg '>
          <p className='font-mplus text-sm'>
            Bukti pembayaran akan dikirim melalui nomor whatsapp yang
            dicantumkan
          </p>
        </div>
        <div className='grid gap-2 rounded bg-white'>
          <label className='text-xs text-black'>Durasi Penggunaan Power</label>
          <div
            type='text'
            className='bg-gray-100 border border-gray-300 text-md font-bold rounded-lg w-full p-2.5 cursor-not-allowed'
            disabled
            readOnly
          >
            <p>{`${bookingService?.data?.duration} Jam`}</p>
          </div>
        </div>
        <div className='border-solid border-b-[1px]'>
          <div className='mb-2'>
            <div className='flex justify-between'>
              <p>Power Socket</p>
              <p>{bookingService?.data?.price}</p>
            </div>
            <p>{`${bookingService?.data?.duration} Jam`}</p>
          </div>
        </div>
        <div className='border-solid border-b-[1px]'>
          <div className='mb-2'>
            <div className='flex justify-between'>
              <p>Subtotal</p>
              <p>{bookingService?.data?.price}</p>
            </div>

            <div className='flex justify-between'>
              <p>Pajak</p>
              <p>{bookingService?.data?.ppn}</p>
            </div>
          </div>
        </div>
        <div className='flex justify-between font-bold text-xl'>
          <p>Total pembayaran</p>
          <p>{bookingService?.data?.price + bookingService?.data?.ppn}</p>
        </div>
      </div>
      <div className='pb-7'>
        <button
          type='submit'
          className='bg-[#F25E39] w-full rounded-3xl p-2 text-white font-bold'
          onClick={handleSubmit(onBookingAndPayment)}
        >
          Bayar dan gunakan sekarang
        </button>
      </div>
    </form>
  )
}

export default Example
