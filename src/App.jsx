import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Scanner from './component/Camera'
import Navbar from './component/Navbar'
import Home from './component/Home'
import Card from './component/Card '

const App = () => {
  return (
    <div className='mx-auto w-full md:w-1/2 lg:w-1/3 xl:w-1/4 font-mplus'>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact>
            <Home />
          </Route>
          <Route path='/qrResult'>
            <Scanner />
          </Route>
          <Route path='/qrTransaction'>
            <Card />
          </Route>
        </Switch>
      </Router>
    </div>
  )
}

export default App
